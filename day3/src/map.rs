pub struct Map {
    tree_pattern: Vec<Vec<bool>>,
    pattern_length: usize,
}

impl Map {
    pub fn is_tree_at(&self, x: usize, y: usize) -> bool {
        self.tree_pattern[y][x % self.pattern_length]
    }

    pub fn from_lines(lines: &Vec<String>) -> Map {
        let mut patterns: Vec<Vec<bool>> = Vec::new();
        for line in lines.iter() {
            patterns.push(
                line.chars()
                    .map(|character| if character == '#' { true } else { false })
                    .collect(),
            )
        }
        let length = patterns.first().expect("No patterns parsed").len();
        Map {
            tree_pattern: patterns,
            pattern_length: length,
        }
    }

    pub fn forest_len(&self) -> usize {
        self.tree_pattern.len()
    }
}
