use std::io::{self, BufRead};

mod map;

use map::Map;

fn count_trees(map: &Map, x_delta: usize, y_delta: usize) -> u32 {
    let mut x = 0;
    let mut y = 0;
    let mut num_trees = 0;
    while y < map.forest_len() {
        if map.is_tree_at(x, y) {
            num_trees += 1;
        }
        x += x_delta;
        y += y_delta;
    }
    num_trees
}

fn main() {
    let lines: Vec<String> = io::stdin()
        .lock()
        .lines()
        .map(|lineres| lineres.expect("Could not read line"))
        .collect();
    let map = Map::from_lines(&lines);
    let slopes = [(1, 1), (3, 1), (5, 1), (7, 1), (1, 2)];
    let mut result = 1;
    for (x, y) in slopes {
        result *= count_trees(&map, x, y);
    }
    println!("{}", result)
}
