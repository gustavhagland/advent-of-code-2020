use std::io::{self, BufRead};
/// Parses a line and returns: (lower bound, upper bound, char and the password)
///
/// The lines must be formatted according to
/// {lower bound}-{upper bound} {character}: {password}
fn parse_line(line: String) -> (u8, u8, char, String) {
    let mut split_line = line.split_whitespace();
    let bounds = split_line.next().expect("Could not extract bounds");
    let mut split_bounds = bounds.split("-");
    let lower_bound: u8 = split_bounds
        .next()
        .expect("Could not parse lower bound")
        .parse()
        .expect("Could not parse lower bound");
    let upper_bound: u8 = split_bounds
        .next()
        .expect("Could not parse upper bound")
        .parse()
        .expect("Could not parse upper bound");
    let character = split_line
        .next()
        .expect("Could not get character")
        .chars()
        .nth(0)
        .expect("character not found");
    let password = split_line.next().expect("No password found");
    return (lower_bound, upper_bound, character, String::from(password));
}

fn is_valid_password_1(
    lower_bound: u8,
    upper_bound: u8,
    character: char,
    password: &String,
) -> bool {
    let mut num_occurounces: u8 = 0;
    for passwd_char in password.chars() {
        if passwd_char == character {
            num_occurounces += 1;
        }
    }
    lower_bound <= num_occurounces && num_occurounces <= upper_bound
}

fn is_valid_password_2(pos1: u8, pos2: u8, character: char, password: &String) -> bool {
    (password
        .chars()
        .nth((pos1 - 1) as usize)
        .expect("No character at pos1")
        == character)
        ^ (password
            .chars()
            .nth((pos2 - 1) as usize)
            .expect("No character at pos2")
            == character)
}

fn main() {
    let lines: Vec<String> = io::stdin()
        .lock()
        .lines()
        .map(|lineres| lineres.expect("COuld not read line"))
        .collect();
    let mut num_valid_passwd_1 = 0;
    let mut num_valid_passwd_2 = 0;
    for line in lines {
        let (num1, num2, character, password) = parse_line(line);
        if is_valid_password_1(num1, num2, character, &password) {
            num_valid_passwd_1 += 1;
        }
        if is_valid_password_2(num1, num2, character, &password) {
            num_valid_passwd_2 += 1;
        }
    }
    println!(
        "Valid passwd according to first criteria: {}",
        num_valid_passwd_1
    );
    println!(
        "Valid passwd according to second criteria: {}",
        num_valid_passwd_2
    )
}
