use std::io::{self, BufRead};
fn main() {
    let lines: Vec<i32> = io::stdin()
        .lock()
        .lines()
        .map(|lineres| lineres.expect("Could not read line"))
        .map(|strline| strline.parse().expect("Could not parse number"))
        .collect();
    for i in 0..lines.len() {
        for j in i + 1..lines.len() {
            if lines[i] + lines[j] == 2020 {
                println!("{}*{}={}", lines[i], lines[j], lines[i] * lines[j])
            }
            for k in j + 1..lines.len() {
                if lines[i] + lines[j] + lines[k] == 2020 {
                    println!(
                        "{}+{}+{}={}",
                        lines[i],
                        lines[j],
                        lines[k],
                        lines[i] * lines[j] * lines[k]
                    )
                }
            }
        }
    }
}
